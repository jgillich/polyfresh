import React, {PureComponent} from 'react'
import Head from 'next/head'

export default class Contact extends PureComponent {
  render() {
    return (
      <div>
        <Head>
          <title key="title">Contact - Polyfresh</title>
        </Head>
        <div className="container mt-4">
          <h2>Contact</h2>
          <form action="https://formspree.io/jakob@gillich.me" method="POST">
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className="form-control" name="name" />
            </div>
            <div className="form-group">
              <label htmlFor="_replyto">Email</label>
              <input type="email" className="form-control" name="_replyto" />
            </div>
            <div className="form-group">
              <label htmlFor="message">Message</label>
              <textarea className="form-control" name="message" rows="5" />
            </div>
            <input className="btn btn-primary" type="submit" value="Send" />
          </form>
        </div>
      </div>
    )
  }
}
