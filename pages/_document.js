import React from 'react'
import Document, {Head, Main, NextScript} from 'next/document'

export default class _Document extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return {...initialProps}
  }

  render() {
    return (
      <html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
            integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
            crossOrigin="anonymous"
          />
          <link rel="stylesheet" href="https://unpkg.com/rheostat@3.0.2/css/rheostat.css" />
          <link rel="icon" type="image/png" href="/static/favicon.png" />
          <meta
            name="description"
            content={`Find the best odor-resistant clothing! We have reviews, comparisons and more. Men & Women.`}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
