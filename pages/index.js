import 'rheostat/initialize'
import React, {Component} from 'react'
import Head from 'next/head'
import {matchArray} from 'searchjs'
import arraySort from 'array-sort'
import qs from 'qs'
import url from 'url'
import cloneDeep from 'lodash/cloneDeep'
import map from 'lodash/map'
import Link from '../components/Link'
import Modal from 'react-bootstrap/lib/Modal'
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger'
import Rating from '../components/Rating'
import {sanity, imageUrl} from '../lib/sanity'
import FilterList from '../components/FilterList'

const query = `
{
  'products': *[_type == 'product']{
    _id, name, "slug": slug.current, listPrice, images, gender, brand, odorControlTechnology, category,
    "rating":
    (
      (count(*[_type=='userReview' && references(^._id) && rating == 1]) * 1) +
      (count(*[_type=='userReview' && references(^._id) && rating == 2]) * 2) +
      (count(*[_type=='userReview' && references(^._id) && rating == 3]) * 3) +
      (count(*[_type=='userReview' && references(^._id) && rating == 4]) * 4) +
      (count(*[_type=='userReview' && references(^._id) && rating == 5]) * 5)
    ) / count(*[_type=='userReview' && references(^._id)])
  },
  'brands': *[_type == 'brand']{..., "slug": slug.current},
  'productCategories': *[_type == 'productCategory'],
  'odorControlTechnologies': *[_type == 'odorControlTechnology']
}
`

export default class Index extends Component {
  static async getInitialProps({req}) {
    const props = await sanity.fetch(query)

    props.minPrice = props.products[0].listPrice
    props.maxPrice = 0

    for (const product of props.products) {
      product.brand = props.brands.find(b => b._id === product.brand._ref)
      product.odorControlTechnology = props.odorControlTechnologies.find(
        o => o._id === product.odorControlTechnology._ref
      )
      product.category = props.productCategories.find(c => c._id === product.category._ref)

      if (!product.rating) {
        product.rating = 0
      }

      if (product.listPrice < props.minPrice) {
        props.minPrice = product.listPrice
      }
      if (product.listPrice > props.maxPrice) {
        props.maxPrice = product.listPrice
      }
    }

    const {search} = req ? url.parse(req.url) : window.location
    props.filters = qs.parse(search, {
      ignoreQueryPrefix: true,
    })
    if (props.filters['listPrice']) {
      props.filters['listPrice'].from = parseInt(props.filters['listPrice'].from, 10)
      props.filters['listPrice'].to = parseInt(props.filters['listPrice'].to, 10)
    }

    return props
  }

  constructor(props) {
    super(props)
    this.state = {
      showFilterModal: false,
      filters: props.filters,
      priceFilter: [props.minPrice, props.maxPrice],
      sort: {
        path: ['brand.slug', 'slug'],
        reverse: false,
      },
    }
  }

  filterAndSort(f, sort, products) {
    const filters = cloneDeep(f)

    if (filters['category.name']) {
      let childCategoryNames = []
      for (const name of filters['category.name']) {
        const category = this.props.productCategories.find(c => c.name === name)
        const children = this.props.productCategories.filter(
          c => c.parentCategory && c.parentCategory._ref === category._id
        )
        childCategoryNames = childCategoryNames.concat(children.map(c => c.name))
      }
      filters['category.name'] = filters['category.name'].concat(childCategoryNames)
    }

    return arraySort(matchArray(products, filters), sort.path, {
      reverse: sort.reverse,
    })
  }

  columns = [
    {
      label: 'Product',
      path: ['brand.slug', 'slug'],
      icon: 'alpha',
      className: 'col mr-auto align-self-center',
      render: product => (
        <div className="row">
          <div className="col-auto align-self-center">
            <OverlayTrigger
              placement="auto"
              delay={{show: 250, hide: 400}}
              overlay={
                <div className="border bg-white" style={{zIndex: 100}}>
                  <img
                    height="500"
                    width="500"
                    alt={product.name}
                    src={imageUrl(product.images[0])
                      .height(500)
                      .url()}
                  />
                </div>
              }
            >
              <img
                style={{cursor: 'zoom-in'}}
                alt={product.name}
                src={imageUrl(product.images[0])
                  .size(50, 50)
                  .ignoreImageParams()
                  .fit('fill')
                  .bg('fff')
                  .url()}
              />
            </OverlayTrigger>
          </div>
          <div className="item-name col mr-auto align-self-center font-weight-bold">
            <Link product={product} withA>
              {product.brand.name} {product.name}
            </Link>
          </div>
        </div>
      ),
    },
    {
      label: 'Price',
      path: 'listPrice',
      icon: 'numeric',
      className: 'col-2 align-self-center d-none d-sm-block',
      render: item => <span>${item.listPrice.toFixed(2)}</span>,
    },
    {
      label: 'Brand',
      path: 'brand.slug',
      icon: 'alpha',
      className: 'col-2 align-self-center d-none d-lg-block',
      render: item => (
        <img
          src={imageUrl(item.brand.image)
            .size(50, 50)
            .ignoreImageParams()
            .fit('fill')
            .bg('fff')
            .url()}
          width="50"
          alt={item.brand.name}
          title={item.brand.name}
        />
      ),
    },
    {
      label: 'Rating',
      path: 'rating',
      icon: 'amount',
      className: 'col-4 col-sm-3 col-md-2 align-self-center',
      reverse: true,
      render: item => <Rating value={item.rating} disabled />,
    },
  ]

  render() {
    const {brands, productCategories, odorControlTechnologies, minPrice, maxPrice} = this.props

    const categories = productCategories
      .filter(c => !c.parentCategory)
      .map(({_id, name}) => ({
        value: name,
        children: productCategories.filter(c => c.parentCategory && c.parentCategory._ref === _id).map(c => c.name),
      }))

    return (
      <div>
        <Head>
          <title key="title">Polyfresh: Find the best odor-resistant clothing</title>
        </Head>

        <div className="container">
          <div className="py-4">
            <h2>Find the best odor-resistant clothing</h2>
            We are an independent review and comparison site about odor-resistant polyester clothing.{' '}
            <Link href="/faq/why-polyester">
              <a href="/faq/why-polyester">Why polyester?</a>
            </Link>
          </div>

          <FilterList>
            <Modal show={this.state.showFilterModal} onHide={this.setState.bind(this, {showFilterModal: false})}>
              <FilterList.Filter hide title="Gender" type="string" path="gender" values={['Men', 'Women']} />
              <FilterList.Filter
                hide
                title="Odor Control"
                type="string"
                path="odorControlTechnology.name"
                values={map(odorControlTechnologies, 'name')}
              />
              <FilterList.Filter hide title="Price" type="range" path="listPrice" min={minPrice} max={maxPrice} />
              <FilterList.Filter hide title="Brand" type="string" path="brand.name" values={map(brands, 'name')} />
              <FilterList.Filter hide title="Category" type="nestedString" path="category.name" values={categories} />
              <Modal.Footer>
                <span className="btn btn-primary" onClick={() => this.setState({showFilterModal: false})}>
                  Apply
                </span>
              </Modal.Footer>
            </Modal>

            <div className="row">
              <div className="col-12 d-md-block d-lg-none pb-2">
                <span className="btn btn-primary btn-block" onClick={() => this.setState({showFilterModal: true})}>
                  Filter
                </span>
              </div>

              <div className="col-3 mb-4 d-none d-lg-block">
                <div className="card border-top-0">
                  <FilterList.Filter title="Gender" type="string" path="gender" values={['Men', 'Women']} />
                  <FilterList.Filter
                    title="Odor Control"
                    type="string"
                    path="odorControlTechnology.name"
                    values={map(odorControlTechnologies, 'name')}
                  />
                  <FilterList.Filter title="Price" type="range" path="listPrice" min={minPrice} max={maxPrice} />
                  <FilterList.Filter title="Brand" type="string" path="brand.name" values={map(brands, 'name')} />
                  <FilterList.Filter title="Category" type="nestedString" path="category.name" values={categories} />
                </div>
              </div>
              <div className="col">
                <FilterList.List
                  items={this.props.products}
                  filterAndSort={this.filterAndSort.bind(this)}
                  columns={this.columns}
                  Link={({item, children}) => <Link product={item}>{children}</Link>}
                />
              </div>
            </div>
          </FilterList>
        </div>
      </div>
    )
  }
}
