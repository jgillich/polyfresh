import '../scss/document.scss'
import React, {createContext} from 'react'
import App, {Container} from 'next/app'
import Router from 'next/router'
import ReactGA from 'react-ga'
import requestIp from 'request-ip'
import fetch from 'isomorphic-unfetch'
import countries from '../sanity/schemas/countries'
import Header from '../components/Header'
import Footer from '../components/Footer'

export const AppContext = createContext()

export default class _App extends App {
  static async getInitialProps({Component, ctx}) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    let country = 'US'
    if (!process.browser) {
      const res = await fetch(
        `https://api.ipdata.co/${requestIp.getClientIp(ctx.req)}?api-key=${process.env.IPDATA_API_KEY}`
      )
      const ipdata = await res.json()
      if (countries.find(c => c.id === ipdata.country_code)) {
        country = ipdata.country_code
      }
    }

    return {pageProps, country}
  }

  constructor(props) {
    super(props)

    this.state = {
      country: process.browser ? localStorage.getItem('country') || props.country : props.country,
      setCountry: this.setCountry.bind(this),
    }
  }

  componentDidMount() {
    if (process.env.NODE_ENV === 'production') {
      ReactGA.initialize('UA-26300821-3')
      ReactGA.set({anonymizeIp: true})
      ReactGA.pageview(window.location.pathname)
      Router.events.on('routeChangeComplete', () => {
        ReactGA.pageview(window.location.pathname)
      })
    }

    Router.events.on('routeChangeStart', () => {
      document.getElementById('progress').style.display = 'flex'
    })
    Router.events.on('routeChangeComplete', () => {
      document.getElementById('progress').style.display = 'none'
    })
    Router.events.on('routeChangeError', () => {
      document.getElementById('progress').style.display = 'none'
    })
  }

  setCountry(country) {
    this.setState({country})
    localStorage.setItem('country', country)
  }

  render() {
    const {Component, pageProps} = this.props

    return (
      <Container>
        <div
          className="justify-content-center align-items-center"
          id="progress"
          style={{
            display: 'none',
            position: 'fixed',
            height: '100vh',
            width: '100vw',
            background: 'rgba(0, 0, 0, 0.5)',
            padding: '100px',
            zIndex: '100',
          }}
        >
          <div className="text-light display-4" style={{height: '100px', width: '100px'}}>
            <div className="spinner-grow" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        </div>
        <AppContext.Provider value={this.state}>
          <Header />
          <Component {...pageProps} />
          <Footer />
        </AppContext.Provider>
      </Container>
    )
  }
}
