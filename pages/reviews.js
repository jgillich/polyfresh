import React, {Component} from 'react'
import Head from 'next/head'
import Link from '../components/Link'
import {sanity} from '../lib/sanity'

const query = `
*[_type == 'review']{
  _id, body[]{..., "asset": asset->}, pros, cons, _createdAt,
  product->{name, gender, "slug": slug.current, images, brand->{name, "slug": slug.current}},
} | order(_createdAt desc)
`

export default class Reviews extends Component {
  static async getInitialProps() {
    return {
      reviews: await sanity.fetch(query),
    }
  }

  constructor(props) {
    super(props)
  }

  render() {
    const {reviews} = this.props

    return (
      <div className="container mt-4">
        <Head>
          <title key="title">Product Reviews - Polyfresh</title>
        </Head>
        <h2>Latest Reviews</h2>
        {reviews.map(review => (
          <div key={review._id}>
            <div className="row my-2">
              <div className="col-lg-8">
                <div className="text-muted">{new Date(review._createdAt).toDateString()}</div>
                <h3>
                  <Link product={review.product} withA>
                    {review.product.brand.name} {review.product.name}
                  </Link>
                </h3>
              </div>
            </div>
          </div>
        ))}
      </div>
    )
  }
}
