import React, {Component} from 'react'
import Head from 'next/head'
import Error from 'next/error'
import {sanity} from '../lib/sanity'

const query = `
*[_type == "faq" && slug.current == $slug] {
  _id,
  question,
  answer,
}[0]
`
export default class Faq extends Component {
  static async getInitialProps({query: {slug}}) {
    return {
      faq: await sanity.fetch(query, {slug}),
    }
  }

  render() {
    const {faq} = this.props
    if (!faq) {
      return <Error statusCode={404} />
    }

    return (
      <div>
        <Head>
          <title key="title">{faq.question} - Polyfresh</title>
        </Head>
        <div className="container mt-4">
          <h2>{faq.question}</h2>
          <div dangerouslySetInnerHTML={{__html: faq.answer}} />
        </div>
      </div>
    )
  }
}
