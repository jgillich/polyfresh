import React from 'react'
import BlockContent from '@sanity/block-content-to-react'
import Head from 'next/head'
import Error from 'next/error'
import Carousel from 'react-bootstrap/lib/Carousel'
import Review from '../components/Review'
import ReviewModal from '../components/ReviewModal'
import Rating from '../components/Rating'
import {sanity, imageUrl} from '../lib/sanity'
import {VigLink} from '../components/VigLink'
import {AppContext} from './_app'

const query = `
*[_type == "product" && slug.current == $slug && gender == $gender]{
  _id, name, listPrice, images, website, gender, description,
  brand->{name, image},
  category->{name, parentCategory->{name}},
  odorControlTechnology->{name},
  "review": *[_type == "review" && references(^._id)]{_id, body[]{..., "asset": asset->}, pros, cons}[0],
  "userReviews": *[_type == "userReview" && references(^._id)]{_id, title, name, rating, body, _createdAt},
  "rating":
    (
      (count(*[_type=='userReview' && references(^._id) && rating == 1]) * 1) +
      (count(*[_type=='userReview' && references(^._id) && rating == 2]) * 2) +
      (count(*[_type=='userReview' && references(^._id) && rating == 3]) * 3) +
      (count(*[_type=='userReview' && references(^._id) && rating == 4]) * 4) +
      (count(*[_type=='userReview' && references(^._id) && rating == 5]) * 5)
    ) / count(*[_type=='userReview' && references(^._id)])
}[0]
`

const starPercentage = (star, reviews) => {
  if (reviews.length === 0) {
    return '0'
  }
  const matchNum = reviews.filter(r => r.rating === star).length
  return ((matchNum / reviews.length) * 100).toFixed(0)
}

export default class Product extends React.Component {
  static async getInitialProps({query: {slug, brand, gender}}) {
    return {
      product: await sanity.fetch(query, {
        slug,
        brand,
        gender: gender.charAt(0).toUpperCase() + gender.slice(1),
      }),
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      showReviewModal: false,
    }
  }

  render() {
    const {product} = this.props
    const {showReviewModal} = this.state

    if (!product) {
      return <Error statusCode={404} />
    }

    return (
      <div>
        <Head>
          <title key="title">
            {product.brand.name} {product.name} Reviews and Information - Polyfresh
          </title>
        </Head>
        <ReviewModal
          product={product}
          show={showReviewModal}
          handleClose={() => this.setState({showReviewModal: false})}
        />
        <div className="my-1 py-2">
          <div className="container">
            <div className="row align-items-center">
              <div className="col">
                <h2 className="mb-0">
                  {product.brand.name} {product.name}
                </h2>
                <Rating value={product.rating} disabled />
                <a href="#reviews" className="ml-2">
                  &nbsp;{product.userReviews.length} {product.userReviews.length === 1 ? 'review' : 'reviews'}
                </a>
              </div>
              <div className="col-auto">
                <img
                  src={imageUrl(product.brand.image)
                    .size(70, 70)
                    .ignoreImageParams()
                    .url()}
                  width="70"
                  alt={product.brand.name}
                  title={product.brand.name}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="py-2">
          <div className="container">
            <div className="row">
              <div className="col-lg-8">
                <Carousel>
                  {product.images.map(image => (
                    <Carousel.Item key={image._key}>
                      <img
                        className="d-block w-100"
                        src={imageUrl(image)
                          .height(800)
                          .url()}
                        alt={product.name}
                      />
                    </Carousel.Item>
                  ))}
                </Carousel>
              </div>

              <div className="col-lg-4">
                <ul className="list-group">
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-auto flex-grow-1">Gender</div>
                      <div className="col-auto">{product.gender}</div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-auto flex-grow-1">Category</div>
                      <div className="col-auto">
                        {product.category.parentCategory ? (
                          <span className="breadcrumb-item">{product.category.parentCategory.name}</span>
                        ) : null}
                        <span className="breadcrumb-item">{product.category.name}</span>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-auto flex-grow-1">Odor Control</div>
                      <div className="col-auto">{product.odorControlTechnology.name}</div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-auto flex-grow-1">List Price</div>
                      <div className="col-auto">${product.listPrice.toFixed(2)}</div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <AppContext.Consumer>
                      {({country}) => (
                        <VigLink
                          href={product.website[country] || product.website.SOURCE}
                          className="btn btn-primary btn-block"
                        >
                          Product website
                        </VigLink>
                      )}
                    </AppContext.Consumer>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className="py-4 col bg-light">
          <div className="container">
            <div className="row">
              <div className="col-lg-8">
                <h3 className="text-uppercase text-center py-2">Manufacturer Description</h3>
                <BlockContent blocks={product.description} />
              </div>
            </div>
          </div>
        </div>

        {product.review ? (
          <div className="py-4">
            <div className="container">
              <div className="row">
                <div className="col-lg-8">
                  <h3 className="text-uppercase text-center py-2">What we think</h3>
                  <Review review={product.review} />
                </div>
              </div>
            </div>
          </div>
        ) : null}

        <div className="container py-4">
          <div className="row">
            <div className="col-lg-8">
              <h3 className="text-uppercase text-center" id="reviews">
                User Ratings & Reviews
              </h3>
            </div>
          </div>
          <div className="row my-4">
            <div className="col-lg-4 text-center">
              <div className="display-4">{(product.rating || 0).toFixed(1)} / 5</div>
              <span className="btn btn-info btn-block" onClick={() => this.setState({showReviewModal: true})}>
                Write a review
              </span>
            </div>

            <div className="col-lg-4 text-center">
              {[5, 4, 3, 2, 1].map(star => (
                <div key={star} className="row align-items-center">
                  <div className="col-3">{star} star</div>
                  <div className="col-6">
                    <div className="progress">
                      <div
                        id="stars-5"
                        className="progress-bar"
                        role="progressbar"
                        style={{
                          width: `${starPercentage(star, product.userReviews)}%`,
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-3 text-right">{starPercentage(star, product.userReviews)}%</div>
                </div>
              ))}
            </div>
          </div>

          <div className="row">
            <div className="col-lg-8">
              <div className="font-weight-bold">
                Showing {product.userReviews.length} {product.userReviews.length === 1 ? 'review' : 'reviews'}
              </div>
              {product.userReviews.map(review => (
                <div key={review._id}>
                  <div className="my-2">
                    <Rating value={review.rating} disabled />
                    <span className="ml-1">
                      {review.name} on {new Date(review._createdAt).toDateString()}
                    </span>
                  </div>
                  <h5>{review.title}</h5>
                  <div className="my-2">{review.body}</div>
                  <hr />
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
