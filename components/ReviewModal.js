import React, {PureComponent} from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import Button from 'react-bootstrap/lib/Button'
import Head from 'next/head'
import Rating from './Rating'

export default class ReviewModal extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  async submit(event) {
    event.preventDefault()
    event.persist()
    this.setState({loading: true})

    const token = await window.grecaptcha.execute('6LfdNYQUAAAAAJI0Yj39TlruP-4ztpYZFdD5CsZi', {
      action: 'submit_review',
    })

    const data = new FormData(event.target)
    data.append('token', token)

    const res = await fetch('/api/userReview', {
      method: 'POST',
      body: data,
    })

    if (res.ok) {
      const {status, message} = await res.json()

      this.setState({
        loading: false,
        message,
        error: !status !== 'success',
        submitted: status === 'success',
      })
    } else {
      this.setState({
        loading: false,
        message: `Server responded with error: ${res.statusText}`,
        error: true,
        submitted: false,
      })
    }
  }

  render() {
    const {product, show, handleClose} = this.props
    return (
      <Modal show={show} onHide={handleClose}>
        <Head>
          <script src="https://www.google.com/recaptcha/api.js?render=6LfdNYQUAAAAAJI0Yj39TlruP-4ztpYZFdD5CsZi" />
          <style type="text/css">{'.grecaptcha-badge { z-index: 2000; }'}</style>
        </Head>
        <Modal.Header closeButton>
          <Modal.Title>Review {product.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.state.submitted ? (
            <div className="alert alert-success" role="alert">
              {this.state.message}
            </div>
          ) : (
            <form onSubmit={this.submit.bind(this)}>
              <label htmlFor="rating">Rating</label>
              <div className="star-rating" name="rating">
                <Rating />
              </div>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input type="title" className="form-control" name="name" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="email" className="form-control" name="email" />
              </div>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input type="title" className="form-control" name="title" />
              </div>
              <div className="form-group">
                <label htmlFor="body">Review</label>
                <textarea className="form-control" name="body" rows="5" />
              </div>
              <p className="d-none">
                <input name="productId" value={product._id} readOnly />
              </p>
              {this.state.error ? (
                <div className="alert alert-danger" role="alert">
                  {this.state.message.split('\n').map(item => (
                    <span key={item}>
                      {item}
                      <br />
                    </span>
                  ))}
                </div>
              ) : null}
            </form>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {this.state.loading ? (
            <Button variant="primary" disabled>
              <i className="fas fa-circle-notch fa-spin" /> Submitting review
            </Button>
          ) : (
            <Button type="submit" className="btn btn-primary" onClick={this.submit.bind(this)}>
              Submit review
            </Button>
          )}
        </Modal.Footer>
      </Modal>
    )
  }
}
