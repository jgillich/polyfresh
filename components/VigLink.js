import React, {PureComponent} from 'react'
import fetch from 'isomorphic-unfetch'
import qs from 'qs'

export class VigLink extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      href: props.href,
    }
  }

  componentDidMount() {
    this.monetize(this.props.href)
  }

  componentDidUpdate(prevProps) {
    if (this.props.href !== prevProps.href) {
      this.monetize(this.props.href)
    }
  }

  async monetize(out) {
    if (process.env.NODE_ENV !== 'production') {
      this.setState({href: out})
      return
    }

    const query = {
      format: 'txt',
      key: process.env.VIGLINK_API_KEY,
      out,
      loc: location.href,
    }

    const res = await fetch(`https://api.viglink.com/api/click?${qs.stringify(query)}`)
    this.setState({href: res.ok ? await res.text() : out})
  }

  render() {
    return (
      <a href={this.state.href} className={this.props.className}>
        {this.props.children}
      </a>
    )
  }
}
