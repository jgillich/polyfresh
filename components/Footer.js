import React from 'react'
import Link from './Link'

const Footer = () => (
  <footer className="mt-5 py-5 text-muted bg-light">
    <div className="container">
      <div className="row">
        <div className="col">
          <Link href="/faq/terms-of-service" withA className="text-muted">
            Terms of Service
          </Link>
          <br />
          <Link href="/faq/privacy-policy" withA className="text-muted">
            Privacy Policy
          </Link>
        </div>
        <div className="col text-center">
          <i className="fas fa-tshirt" />
        </div>
        <div className="col text-right">
          <Link href="/contact" withA className="text-muted">
            Contact
          </Link>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
