import React, {PureComponent} from 'react'
import Link from './Link'
import {withRouter} from 'next/router'
import {Navbar, Nav, Dropdown} from 'react-bootstrap'
import arraySort from 'array-sort'
import sourceCountries from '../sanity/schemas/countries'
import {AppContext} from '../pages/_app'

const countries = arraySort(sourceCountries, ['title'])

export default withRouter(
  class Header extends PureComponent {
    render() {
      const {router} = this.props
      return (
        <Navbar bg="dark" variant="dark" expand="lg">
          <div className="container">
            <Link href="/">
              <Navbar.Brand href="/">
                <i className="fas fa-tshirt" />
                {' Polyfresh'}
              </Navbar.Brand>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Link href="/reviews">
                  <Nav.Link active={router.pathname === '/reviews'} href="/reviews">
                    Reviews
                  </Nav.Link>
                </Link>
              </Nav>
              <Navbar.Collapse className="justify-content-end">
                <AppContext.Consumer>
                  {({country, setCountry}) => (
                    <Dropdown>
                      <Dropdown.Toggle variant="dark" id="dropdown-basic">
                        {country}
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {countries.map(c => (
                          <Dropdown.Item key={c.id} onClick={() => setCountry(c.id)}>
                            <img src={`https://www.countryflags.io/${c.id.toLowerCase()}/flat/32.png`} alt={c.id} />{' '}
                            {c.title}
                          </Dropdown.Item>
                        ))}
                      </Dropdown.Menu>
                    </Dropdown>
                  )}
                </AppContext.Consumer>
              </Navbar.Collapse>
            </Navbar.Collapse>
          </div>
        </Navbar>
      )
    }
  }
)
