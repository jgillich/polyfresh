import React, {Component, Fragment, createContext} from 'react'
import Rheostat from 'rheostat'
import includes from 'lodash/includes'
import get from 'lodash/get'
import isEqual from 'lodash/isEqual'
import cloneDeep from 'lodash/cloneDeep'
import pull from 'lodash/pull'
import memoize from 'memoize-one'
import Collapse from 'react-bootstrap/lib/Collapse'

const Context = createContext()

const Filter = class extends Component {
  constructor(props) {
    super(props)

    this.state = {hide: !!props.hide}

    switch (props.type) {
      case 'range':
        this.state.displayValues = [props.min, props.max]
        break
    }
  }

  build = memoize(({type, path, values, min, max}, {displayValues}, setState) => {
    const Checkbox = ({className = '', value, filters, filterChange}) => (
      <div className={`py-1 ${className}`} style={{cursor: 'pointer'}} onClick={() => filterChange(type, path, value)}>
        {includes(filters[path], value) ? <i className="far fa-check-square" /> : <i className="far fa-square" />}
        {`  ${value}`}
      </div>
    )

    switch (type) {
      case 'nestedString':
        return (
          <Fragment>
            <Context.Consumer>
              {({filters, filterChange}) =>
                values.map(value => (
                  <Fragment key={value.value}>
                    <Checkbox value={value.value} filters={filters} filterChange={filterChange} />
                    {value.children.map(value => (
                      <Checkbox
                        className="pl-2"
                        key={value}
                        value={value}
                        filters={filters}
                        filterChange={filterChange}
                      />
                    ))}
                  </Fragment>
                ))
              }
            </Context.Consumer>
          </Fragment>
        )
      case 'string':
        return (
          <Fragment>
            <Context.Consumer>
              {({filters, filterChange}) =>
                values.map(value => (
                  <Checkbox key={value} value={value} filters={filters} filterChange={filterChange} />
                ))
              }
            </Context.Consumer>
          </Fragment>
        )
      case 'range':
        return (
          <Fragment>
            <div className="text-center mb-3">{`$${displayValues[0].toFixed(2)} - $${displayValues[1].toFixed(
              2
            )}`}</div>
            <Context.Consumer>
              {({filters, filterChange}) => (
                <Rheostat
                  onValuesUpdated={({values}) => setState({displayValues: values})}
                  onChange={({values}) => filterChange(type, path, {from: values[0], to: values[1]})}
                  min={min}
                  max={max}
                  values={[get(filters, `${path}.from`, min), get(filters, `${path}.to`, max)]}
                />
              )}
            </Context.Consumer>
          </Fragment>
        )
      default:
        throw new Error(`unsupported type ${type}`)
    }
  }, isEqual)

  render() {
    return (
      <div className="card-body border-top">
        <div style={{cursor: 'pointer'}} onClick={() => this.setState({hide: !this.state.hide})}>
          <span className="font-weight-bold">{this.props.title}</span>
          <span className="ml-auto float-right">
            <i className={`fas ${this.state.hide ? 'fa-angle-down' : 'fa-angle-up'}`} />
          </span>
        </div>
        <Collapse in={!this.state.hide}>
          {/* container is needed for animation to play smoothly */}
          <div>
            <div className="mt-2">{this.build(this.props, this.state, this.setState.bind(this))}</div>
          </div>
        </Collapse>
      </div>
    )
  }
}

const List = class extends Component {
  render() {
    const {columns, items, Link} = this.props
    return (
      <span>
        <style jsx>{`
          .item:hover {
            text-decoration: underline;
          }
        `}</style>
        <div className="row font-weight-bold">
          {columns.map(column => (
            <Context.Consumer key={column.label}>
              {({sort, sortChange}) => (
                <div
                  className={column.className}
                  style={{cursor: 'pointer'}}
                  onClick={() => sortChange(column.path, column.reverse)}
                >
                  {`${column.label}  `}
                  {isEqual(column.path, sort.path) ? (
                    sort.reverse ? (
                      <i className={`fas fa-sort-${column.icon}-up`} />
                    ) : (
                      <i className={`fas fa-sort-${column.icon}-down`} />
                    )
                  ) : null}
                </div>
              )}
            </Context.Consumer>
          ))}
        </div>
        <hr />
        <Context.Consumer>
          {({sort, filters}) =>
            this.props.filterAndSort(filters, sort, items).map(item => (
              <div key={item._id} className="border-bottom border-light pb-2 mb-2">
                <Link item={item}>
                  <div className="row item" style={{cursor: 'pointer'}}>
                    {columns.map(column => (
                      <div key={column.label} className={column.className}>
                        {column.render(item)}
                      </div>
                    ))}
                  </div>
                </Link>
              </div>
            ))
          }
        </Context.Consumer>
      </span>
    )
  }
}

const FilterList = class extends Component {
  static Filter = Filter
  static List = List

  constructor(props) {
    super(props)

    this.state = {
      filters: props.filters || {},
      filterChange: this.filterChange.bind(this),
      sort: {
        path: ['brand.slug', 'slug'],
        reverse: false,
      },
      sortChange: this.sortChange.bind(this),
    }
  }

  filterChange(type, path, value) {
    const filters = cloneDeep(this.state.filters)

    switch (type) {
      case 'nestedString':
      case 'string':
        if (!filters[path]) {
          filters[path] = []
        }
        if (filters[path].includes(value)) {
          pull(filters[path], value)
          if (filters[path].length === 0) {
            delete filters[path]
          }
        } else {
          filters[path].push(value)
        }
        break
      case 'range':
        if (value) {
          filters[path] = value
        } else {
          delete filters[path]
        }
        break
    }

    this.setState({filters})
  }

  sortChange(path, reverse = false) {
    this.setState({
      sort: {
        path,
        reverse: isEqual(path, this.state.sort.path) ? !this.state.sort.reverse : reverse,
      },
    })
  }

  render() {
    const {children} = this.props
    return (
      <div>
        <Context.Provider value={this.state}>{children}</Context.Provider>
      </div>
    )
  }
}

export default FilterList
