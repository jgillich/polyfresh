import React, {PureComponent} from 'react'
import BlockContent from '@sanity/block-content-to-react'
import {imageUrl} from '../lib/sanity'
import get from 'lodash/get'

const serializers = {
  types: {
    image: props => {
      return (
        <img
          src={imageUrl(props.node.asset)
            .width(800)
            .url()}
          className="img-fluid"
          alt="Product"
        />
      )
    },
  },
}

export default class Review extends PureComponent {
  render() {
    const {review} = this.props
    return (
      <div>
        <div className="my-2">
          <BlockContent blocks={review.body} serializers={serializers} />
        </div>
        <div className="row justify-content-center">
          <div className="col-sm-auto pr-lg-5">
            <h4 className="text-success">
              <i className="fas fa-plus-circle" /> Pros
            </h4>
            {get(review, 'pros', []).map(pro => (
              <div key={pro} className="lead">
                {pro}
              </div>
            ))}
          </div>
          <div className="col-sm-auto pl-lg-5">
            <h4 className="text-danger">
              <i className="fas fa-minus-circle" /> Cons
            </h4>
            {get(review, 'cons', []).map(con => (
              <div key={con} className="lead">
                {con}
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
