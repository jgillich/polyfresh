import React, {PureComponent} from 'react'

export default class Rating extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      value: props.value || 0,
      displayValue: props.value || 0,
    }
  }

  disabled = false

  set(value) {
    if (this.props.disabled) return
    this.setState({
      value,
      displayValue: value,
    })
  }

  mouseover(value) {
    if (this.props.disabled) return
    this.setState({displayValue: value})
  }

  mouseout() {
    this.setState({displayValue: this.state.value})
  }

  render() {
    const stars = []

    for (let i = 1; i <= 5; i++) {
      let className = 'd-inline-block '

      if (this.state.displayValue >= i - 0.33) {
        className = 'fas fa-star'
      } else if (this.state.displayValue >= i - 0.66) {
        className = 'fas fa-star-half-alt'
      } else {
        className = 'far fa-star'
      }

      stars.push(
        <i
          key={`star${i}`}
          style={this.props.disabled ? {} : {cursor: 'pointer'}}
          className={className}
          onClick={this.set.bind(this, i)}
          onMouseOver={this.mouseover.bind(this, i)}
          onMouseOut={this.mouseout.bind(this)}
        />
      )
    }

    return (
      <span className="text-nowrap">
        {stars}
        <input type="hidden" name="rating" value={this.state.value} />
      </span>
    )
  }
}
