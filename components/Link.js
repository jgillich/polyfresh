import React, {PureComponent} from 'react'
import NextLink from 'next/link'
import memoize from 'memoize-one'

// TODO importing now.json fails, but it shouldn't.
const routes = [
  {
    src: '^/men/([\\w-]+)/([\\w-]+)$',
    dest: 'product?gender=men&brand=$1&slug=$2',
  },
  {
    src: '^/women/([\\w-]+)/([\\w-]+)$',
    dest: 'product?gender=women&brand=$1&slug=$2',
  },
  {
    src: '^/api/([\\w-]+)',
    dest: 'api/$1.js',
  },
  {
    src: '^/faq/([\\w-]+)$',
    dest: 'faq?slug=$1',
  },
]

export default class Link extends PureComponent {
  page = memoize(url => {
    for (const {src, dest} of routes) {
      const re = new RegExp(src)
      const match = url.match(re)
      if (match) {
        return url.replace(re, `/${dest}`)
      }
    }
    return url
  })

  href() {
    const {product, href} = this.props
    if (href) {
      return href
    } else if (this.props.product) {
      return `/${product.gender.toLowerCase()}/${product.brand.slug}/${product.slug}`
    }
  }

  render() {
    const {children, withA, className} = this.props
    const href = this.href()
    const page = this.page(href)
    const as = process.env.NODE_ENV === 'production' ? href : page
    return (
      <NextLink as={as} href={page}>
        {withA ? (
          <a href={href} className={className}>
            {children}
          </a>
        ) : (
          children
        )}
      </NextLink>
    )
  }
}
