const {send} = require('micro')
const qs = require('qs')
const fetch = require('node-fetch')
const formidable = require('formidable')
const sanityClient = require('@sanity/client')
const Joi = require('joi')
const post = require('micro-post')

const client = sanityClient({
  projectId: process.env.SANITY_PROJECT_ID,
  dataset: process.env.SANITY_DATASET,
  token: process.env.SANITY_TOKEN,
  useCdn: false,
})

const schema = Joi.object()
  .keys({
    _type: Joi.any(),
    product: Joi.any(),
    rating: Joi.number()
      .integer()
      .min(1)
      .max(5)
      .required(),
    email: Joi.string()
      .email()
      .required(),
    name: Joi.string()
      .min(2)
      .max(80)
      .required(),
    title: Joi.string()
      .min(2)
      .max(80)
      .required(),
    body: Joi.string()
      .min(2)
      .max(1000)
      .required(),
  })
  .with('username', 'birthyear')
  .without('password', 'access_token')

module.exports = post(async (req, res) => {
  const submission = await new Promise((resolve, reject) => {
    new formidable.IncomingForm().parse(req, function(err, fields) {
      err ? reject(err) : resolve(fields)
    })
  })

  const query = qs.stringify({
    secret: process.env.GRECAPTCHA_SECRET,
    response: submission.token,
  })

  const verify = await (await fetch(`https://www.google.com/recaptcha/api/siteverify?${query}`)).json()

  if (!verify.success || verify.score <= 0.3) {
    send(res, 403, {
      status: 'error',
      message: 'Sorry, we believe you are a bot.',
    })
    return
  }

  const doc = {
    _type: 'userReview',
    rating: parseInt(submission.rating, 10),
    email: submission.email,
    name: submission.name,
    title: submission.title,
    body: submission.body,
    product: {
      _ref: submission.productId,
      _type: 'reference',
    },
  }

  const {error} = Joi.validate(doc, schema)

  if (error) {
    send(res, 400, {
      status: 'error',
      message: error.details.map(d => d.message).join('\n'),
    })
  } else {
    try {
      await client.create(doc)
      send(res, 200, {
        status: 'success',
        message: 'Your review was submitted. Thank you!',
      })
    } catch (error) {
      send(res, 500, {
        status: 'error',
        message: 'Internal server error.',
      })
    }
  }
})
