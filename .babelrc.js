require('now-env')

const env = {
  'process.env.NODE_ENV': process.env.NODE_ENV,
  'process.env.SANITY_PROJECT_ID': process.env.SANITY_PROJECT_ID,
  'process.env.SANITY_DATASET': process.env.SANITY_DATASET,
  'process.env.VIGLINK_API_KEY': process.env.VIGLINK_API_KEY,
  'process.env.IPDATA_API_KEY': process.env.IPDATA_API_KEY,
}

module.exports = {
  presets: ['next/babel'],
  plugins: [['transform-define', env], ['preval']],
}
