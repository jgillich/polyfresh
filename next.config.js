const webpack = require('webpack')
const withBundleAnalyzer = require('@zeit/next-bundle-analyzer')
const withSass = require('@zeit/next-sass')

module.exports = withBundleAnalyzer(
  withSass({
    target: 'serverless',
    analyzeServer: ['server', 'both'].includes(process.env.BUNDLE_ANALYZE),
    analyzeBrowser: ['browser', 'both'].includes(process.env.BUNDLE_ANALYZE),
    bundleAnalyzerConfig: {
      server: {
        analyzerMode: 'static',
        reportFilename: '/tmp/next-bundle/server.html',
      },
      browser: {
        analyzerMode: 'static',
        reportFilename: '/tmp/next-bundle/browser.html',
      },
    },
    webpack(config) {
      // fix webpack warning (https://github.com/andris9/encoding/issues/16)
      config.plugins.push(new webpack.IgnorePlugin(/\/iconv-loader$/))
      return config
    },
  })
)
