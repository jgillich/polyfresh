const github = require('eslint-plugin-github/prettier.config')

module.exports = {
  ...github,
  trailingComma: 'es5',
}
