import sanityClient from '@sanity/client'
import imageUrlBuilder from '@sanity/image-url'

export const sanity = sanityClient({
  projectId: process.env.SANITY_PROJECT_ID,
  dataset: process.env.SANITY_DATASET,
  useCdn: process.env.NODE_ENV === 'production',
})

const imageBuilder = imageUrlBuilder(sanity)

export function imageUrl(source) {
  return imageBuilder.image(source)
}
