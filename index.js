require('now-env')
const next = require('next')
const {parse} = require('url')
const {existsSync} = require('fs')
const {routes} = require('./now.json')
const dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

module.exports = async (req, res) => {
  await app.prepare()

  const url = req.url

  for (const {src, dest} of routes) {
    const re = new RegExp(src)
    const match = url.match(re)

    if (match) {
      const to = url.replace(re, `/${dest}`)

      if (to.endsWith('.js') && existsSync(`.${to}`)) {
        require(`.${url}`)(req, res)
        delete require.cache[require.resolve(`.${url}`)]
        return
      } else {
        const {pathname, query} = parse(to, true)
        return app.render(req, res, pathname, query)
      }
    }
  }

  return handle(req, res, parse(url, true))
}
