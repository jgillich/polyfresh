import countries from './countries'

export default {
  name: 'countryString',
  type: 'object',
  fieldsets: [
    {
      title: 'Countries',
      name: 'countries',
      options: {collapsible: true},
    },
  ],
  fields: [
    {
      name: 'SOURCE',
      title: 'Source',
      type: 'string',
      validation: Rule => Rule.required(),
    },
  ].concat(
    countries.map(country => ({
      title: country.title,
      name: country.id,
      type: 'string',
      fieldset: 'countries',
    }))
  ),
}
