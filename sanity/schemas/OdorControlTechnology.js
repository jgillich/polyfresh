export default {
  name: 'odorControlTechnology',
  title: 'Odor Control Technology',
  type: 'document',
  preview: {
    select: {
      title: 'name',
    },
  },
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      validation: Rule => Rule.required(),
      type: 'slug',
      options: {
        source: 'name',
      },
    },
    {
      name: 'website',
      title: 'Website',
      type: 'url',
    },
  ],
}
