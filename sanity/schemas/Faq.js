export default {
  name: 'faq',
  title: 'FAQ',
  type: 'document',
  preview: {
    select: {
      title: 'question',
    },
  },
  fields: [
    {
      name: 'question',
      title: 'Question',
      validation: Rule => Rule.required(),
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      validation: Rule => Rule.required(),
      type: 'slug',
      options: {
        source: 'question',
      },
    },
    {
      name: 'answer',
      title: 'Answer',
      validation: Rule => Rule.required(),
      type: 'text',
    },
  ],
}
