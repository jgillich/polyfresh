export default {
  name: 'userReview',
  title: 'User Review',
  type: 'document',
  preview: {
    select: {
      title: 'title',
    },
  },
  fields: [
    {
      name: 'title',
      title: 'Title',
      validation: Rule => Rule.required(),
      type: 'string',
    },
    {
      name: 'name',
      title: 'Name',
      validation: Rule => Rule.required(),
      type: 'string',
    },
    {
      name: 'email',
      title: 'Email',
      validation: Rule => Rule.required(),
      type: 'string',
    },
    {
      name: 'rating',
      title: 'Rating',
      validation: Rule => Rule.required(),
      type: 'number',
    },
    {
      name: 'body',
      title: 'Body',
      validation: Rule => Rule.required(),
      type: 'text',
    },
    {
      name: 'product',
      title: 'Product',
      validation: Rule => Rule.required(),
      type: 'reference',
      to: [{type: 'product'}],
    },
  ],
}
