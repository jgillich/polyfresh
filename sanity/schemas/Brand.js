export default {
  name: 'brand',
  title: 'Brand',
  type: 'document',
  preview: {
    select: {
      title: 'name',
    },
  },
  fields: [
    {
      name: 'name',
      title: 'Name',
      validation: Rule => Rule.required(),
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      validation: Rule => Rule.required(),
      type: 'slug',
      options: {
        source: 'name',
      },
    },
    {
      name: 'image',
      title: 'Image',
      type: 'image',
    },
    {
      name: 'website',
      title: 'Website',
      type: 'url',
    },
  ],
}
