export default {
  name: 'productCategory',
  title: 'Product Category',
  type: 'document',
  preview: {
    select: {
      title: 'name',
    },
  },
  fields: [
    {
      name: 'name',
      title: 'Name',
      validation: Rule => Rule.required(),
      type: 'string',
    },
    {
      name: 'parentCategory',
      title: 'Parent Category',
      type: 'reference',
      to: [
        {
          type: 'productCategory',
        },
      ],
    },
  ],
}
