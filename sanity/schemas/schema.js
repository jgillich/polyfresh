import createSchema from 'part:@sanity/base/schema-creator'
import schemaTypes from 'all:part:@sanity/base/schema-type'
import Brand from './Brand'
import Product from './Product'
import OdorControlTechnology from './OdorControlTechnology'
import Review from './Review'
import UserReview from './UserReview'
import ProductCategory from './ProductCategory'
import Faq from './Faq'
import countryString from './countryString'

export default createSchema({
  name: 'default',
  types: schemaTypes.concat([
    countryString,
    Brand,
    Product,
    OdorControlTechnology,
    Review,
    UserReview,
    ProductCategory,
    Faq,
  ]),
})
