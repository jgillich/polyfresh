export default {
  name: 'review',
  title: 'Review',
  type: 'document',
  preview: {
    select: {
      title: 'product.name',
    },
  },
  fields: [
    {
      name: 'product',
      title: 'Product',
      validation: Rule => Rule.required(),
      type: 'reference',
      to: [{type: 'product'}],
    },
    {
      name: 'body',
      title: 'Body',
      validation: Rule => Rule.required(),
      type: 'array',
      of: [{type: 'block'}, {type: 'image'}],
    },
    {
      title: 'Pros',
      name: 'pros',
      type: 'array',
      of: [{type: 'string'}],
    },
    {
      title: 'Cons',
      name: 'cons',
      type: 'array',
      of: [{type: 'string'}],
    },
  ],
}
