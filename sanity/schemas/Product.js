export default {
  name: 'product',
  title: 'Product',
  type: 'document',
  preview: {
    select: {
      title: 'name',
      subtitle: 'brand.name',
    },
  },
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      validation: Rule => Rule.required(),
      type: 'slug',
      options: {
        source: 'name',
        isUnique: (slug, objects) => true, // TODO
      },
    },
    {
      name: 'listPrice',
      title: 'List Price',
      type: 'number',
    },
    {
      name: 'images',
      title: 'Images',
      type: 'array',
      of: [
        {
          type: 'image',
        },
      ],
    },
    {
      name: 'brand',
      title: 'Brand',
      validation: Rule => Rule.required(),
      type: 'reference',
      to: [
        {
          type: 'brand',
        },
      ],
    },
    {
      name: 'website',
      title: 'Website',
      validation: Rule => Rule.required(),
      type: 'countryString',
    },
    {
      name: 'gender',
      title: 'Gender',
      validation: Rule => Rule.required(),
      type: 'string',
      options: {
        list: ['Men', 'Women', 'Unisex'],
        layout: 'radio',
      },
    },
    {
      name: 'category',
      title: 'Category',
      validation: Rule => Rule.required(),
      type: 'reference',
      to: [
        {
          type: 'productCategory',
        },
      ],
    },
    {
      name: 'odorControlTechnology',
      title: 'Odor Control Technology',
      validation: Rule => Rule.required(),
      type: 'reference',
      to: [
        {
          type: 'odorControlTechnology',
        },
      ],
    },
    {
      name: 'description',
      title: 'Description',
      validation: Rule => Rule.required(),
      type: 'array',
      of: [{type: 'block'}],
    },
  ],
}
