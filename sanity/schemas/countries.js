// @preval
const find = require('lodash/find')
const worldCountries = require('world-countries')

const countries = [
  {id: 'US'},
  {id: 'CA'},
  {id: 'MX'},
  {id: 'GB'},
  {id: 'DE'},
  {id: 'FR'},
  {id: 'IT'},
  {id: 'ES'},
  {id: 'AT'},
  {id: 'NL'},
  {id: 'AU'},
]

for (const country of countries) {
  const data = find(worldCountries, {cca2: country.id})
  country.title = data.name.common
  country.tld = data.tld
}

module.exports = countries
