/* eslint no-console: 0 */
import 'now-env'
import countries from '../sanity/schemas/countries'
import sanityClient from '@sanity/client'
import {URL} from 'url'
import puppeteer from 'puppeteer'
import chalk from 'chalk'

const client = sanityClient({
  projectId: process.env.SANITY_PROJECT_ID,
  dataset: process.env.SANITY_DATASET,
  token: process.env.SANITY_TOKEN,
  useCdn: false,
})

const query = '*[_type == "product"]{_id, "slug": slug.current, website}'

async function resolve(page, rawUrl, validate, noRetry = false) {
  const url = new URL(rawUrl)
  try {
    const res = await page.goto(url, {waitUntil: 'networkidle2'})
    if (res.ok()) {
      if (!validate || validate(page.url())) {
        console.log(`${chalk.green('found')} ${url} -> ${page.url()}`)
        return page.url()
      } else {
        console.log(`${chalk.grey('invalid')}  ${url} -> ${page.url()}`)
        return null
      }
    } else {
      if (res.status() === 404) {
        console.log(`${chalk.grey('404')} ${url} -> ${page.url()}`)
        return null
      } else {
        if (!noRetry) {
          page.waitFor(5000 * Math.random())
          return await resolve(page, url, validate, true)
        }
        console.log(`${chalk.red('bad response')} (${res.status()}) ${url} -> ${page.url()}`)
        return null
      }
    }
  } catch (err) {
    if (!noRetry) {
      console.log(`${chalk.grey('retrying')} (${err}) ${url} -> ${page.url()}`)
      return await resolve(page, url, validate, true)
    }
    console.log(`${chalk.red('error')} (${err}) ${url} -> ${page.url()}`)
    return null
  }
}

;(async () => {
  let exitCode = 0

  const browser = await puppeteer.launch({
    args: process.env.CI ? ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage'] : [],
  })

  async function withPage(fn) {
    const page = await browser.newPage()
    await page.setUserAgent(
      'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3202.75 Safari/537.36'
    )
    await page.setJavaScriptEnabled(false)
    await fn(page)
    await page.close()
  }

  try {
    const products = await client.fetch(query)

    for (const product of products) {
      product.url = new URL(product.website.SOURCE)
      product.originalWebsite = product.website
      product.website = domainMap[product.url.hostname] ? domainMap[product.url.host].map(product.url) : {}
    }

    for (const country of countries) {
      await Promise.all(
        products
          .filter(p => !!p.website[country.id])
          .map(product =>
            (async function(product, country) {
              await withPage(async page => {
                const url = await resolve(page, product.website[country.id], domainMap[product.url.hostname].validate)
                if (url) {
                  product.website[country.id] = url
                } else {
                  delete product.website[country.id]
                }
              })
            })(product, country)
          )
      )
    }

    await withPage(async page => {
      for (const {_id, url, originalWebsite, website} of products) {
        for (const langId of Object.keys(originalWebsite)) {
          if (countries.find(({id}) => langId === id) && !website[langId]) {
            const url = await resolve(page, originalWebsite[langId])
            if (!url) {
              exitCode = 1
              console.error(`FIXME broken url ${_id}/${langId} ${originalWebsite[langId]}`)
            } else if (url !== originalWebsite[langId]) {
              exitCode = 1
              console.error(`FIXME redirecting url ${_id}/${langId} ${originalWebsite[langId]} -> ${url}`)
            }
          }
        }

        if (domainMap[url.hostname]) {
          await client
            .patch(_id)
            .set({website: {...originalWebsite, ...website}})
            .commit()
        }
      }
    })
  } catch (err) {
    console.log(err)
    exitCode = 1
  } finally {
    await browser.close()
    process.exit(exitCode)
  }
})()

const domainMap = {
  'www.adidas.com': {
    map(sourceUrl) {
      const productId = /\/(\w+).html$/.exec(sourceUrl.pathname)[1]
      return countries
        .map(({id, tld}) => {
          const url = new URL(sourceUrl.toString())
          url.pathname = `${productId}.html`

          switch (id) {
            case 'US':
              url.href = url.href.replace('.com', '.com/us')
              break
            case 'CA':
              url.href = url.href.replace('.com', '.ca/en')
              break
            case 'GB':
              url.href = url.href.replace('.com', '.co.uk')
              break
            case 'AU':
              url.href = url.href.replace('.com', '.com.au')
              break
            default:
              url.href = url.href.replace('.com', tld[0])
          }
          return [id, url.toString()]
        })
        .reduce((a, v) => ({...a, [v[0]]: v[1]}), {})
    },
    validate: url => /\/(\w+).html/.test(url),
  },
  'www.decathlon.co.uk': {
    map(sourceUrl) {
      return countries
        .map(({id, tld}) => {
          const url = new URL(sourceUrl.toString())
          let domain = tld[0]
          switch (id) {
            case 'GB':
              domain = '.co.uk'
              break
            case 'AU':
              domain = '.com.au'
              break
            // these don't have decathlon
            case 'MX':
              return null
            // uses a different shop system
            case 'US':
            case 'CA':
              return null
          }
          url.hostname = `www.decathlon${domain}`
          return [id, url.toString()]
        })
        .filter(v => v !== null)
        .reduce((a, v) => ({...a, [v[0]]: v[1]}), {})
    },
    validate: url => /(\d+).html/.test(url),
  },
  'www.uniqlo.com': {
    map(sourceUrl) {
      const productId = /-(\d+).html$/.exec(sourceUrl.pathname)[1]
      return countries
        .map(({id}) => {
          const url = new URL(sourceUrl.toString())
          // en is fine because it will be redirected if it doesn't exist
          url.pathname = `/en/${productId}.html`

          switch (id) {
            case 'US':
              url.pathname = `/us/en/${productId}.html`
              break
            // these don't have uniqlo
            case 'AT':
            case 'CA':
            case 'MX':
            case 'IT':
            case 'NL':
              return null
            // uses a different shop system
            case 'AU':
              return null
            case 'GB':
              url.pathname = `/uk${url.pathname}`
              break
            default:
              url.pathname = `/${id.toLowerCase()}${url.pathname}`
              break
          }

          return [id, url.toString()]
        })
        .filter(v => v !== null)
        .reduce((a, v) => ({...a, [v[0]]: v[1]}), {})
    },
    validate: url => /(\d+).html/.test(url),
  },
}
